# registryFuture - provides managed concurrent processing for registered functions, with the Future interface allowing later collection of the results of the invocation.

The motivation of this package is to simplify the management of concurrent processing via goroutines, allowing multiple goroutines to block and receive the results of a 
given invocation of a registered function.  The package handles the channel communications between the routines, making code simpler to read whilst handling the complexities
of multi-plexing the result of the function call to all waiting goroutines.

## Getting started

Current version of the library requires a latest stable Go release. If you don't have the Go compiler installed, read the official [Go install guide](http://golang.org/doc/install).

Use go tool to install the package in your packages tree:

```
go get bitbucket.com/gford10000/registryFuture
```

Then you can use it in import section of your Go programs:

```go
import "bitbucket.com/gford10000/functionRegistry"
```

## Basic Example

```go
package main

import (
	"bitbucket.com/gford10000/functionRegistry"
	"bitbucket.com/gford10000/registryFuture"
	"fmt"
	"strings"
)

// hello is the function to be called
func hello(names ...string) string {
	if len(names) == 0 {
		return "Hello, World"
	}

	return "Hello " + strings.Join(names, ", ")
}

// initialise would typically be called at startup
func initialise() {
	functionRegistry.Register("", "HelloFn", hello)	
}

func main() {

	initialise()

	future := registryFuture.New("", "HelloFn", "Simon", "David")

	result, _ := future.GetResult() // Block to receive result

	v, _ := result.Value(0) // retrieve value from Result

	fmt.Printf("%v\n", v)	// Prints Hello Simon, David
}
```

