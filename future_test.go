// Copyright 2014 Geoff Ford. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package registryFuture

import (
	"bitbucket.com/gford10000/functionRegistry"
	"runtime"
	"strings"
	"testing"
	"time"
)

func registerFunctions() {

	f1 := func() string {
		return "Hello, World"
	}

	f2 := func(names ...string) string {
		return "Hello " + strings.Join(names, ", ")
	}

	f3 := func(vals ...int) int {
		total := 0
		for i := range vals {
			total += vals[i]
		}
		return total
	}

	f4 := func(lower float64, upper float64) float64 {
		var total float64 = 0
		for i := lower; i <= upper; i++ {
			total += i
		}
		return total
	}

	functionRegistry.Register("Testing", "F1", f1)
	functionRegistry.Register("Testing", "F2", f2)
	functionRegistry.Register("Testing", "F3", f3)
	functionRegistry.Register("Testing", "F4", f4)
}

func Test1(t *testing.T) {

	registerFunctions()

	future := New("Testing", "F1")

	r, e := future.GetResult()

	if e != nil {
		t.Errorf("Unexpected error returned - %v", e)
	} else if r.NumValues() != 1 {
		t.Errorf("Unexpected number of values returned - %v", r.NumValues())
	} else {
		v0, e := r.Value(0)
		if e != nil {
			t.Errorf("Unexpected error getting value - %v", e)
		} else {
			if s, ok := v0.(string); ok && s != "Hello, World" {
				t.Errorf("Incorrect value returned - expected 'Hello, World', got '%v'", s)
			}
		}
	}
}

func Test2(t *testing.T) {

	registerFunctions()

	future := New("Testing", "F1", 1, "Hello")

	_, e := future.GetResult()

	if e == nil {
		t.Errorf("Expected error but none returned")
	} else {
		errMsg := "Too many args supplied: received 2, expected 0"
		if e.Error() != errMsg {
			t.Errorf("Expected error %v, but got %v", errMsg, e.Error())
		}
	}
}

func Test3(t *testing.T) {

	registerFunctions()

	future := New("Testing", "F2")

	r, e := future.GetResult()

	if e != nil {
		t.Errorf("Unexpected error returned - %v", e)
	} else if r.NumValues() != 1 {
		t.Errorf("Unexpected number of values returned - %v", r.NumValues())
	} else {
		v0, e := r.Value(0)
		if e != nil {
			t.Errorf("Unexpected error getting value - %v", e)
		} else {
			if s, ok := v0.(string); ok && s != "Hello " {
				t.Errorf("Incorrect value returned - expected 'Hello , got '%v'", s)
			}
		}
	}
}

func Test4(t *testing.T) {

	registerFunctions()

	future := New("Testing", "F2", "Colin", "John", "Fred")

	r, e := future.GetResult()

	if e != nil {
		t.Errorf("Unexpected error returned - %v", e)
	} else if r.NumValues() != 1 {
		t.Errorf("Unexpected number of values returned - %v", r.NumValues())
	} else {
		v0, e := r.Value(0)
		if e != nil {
			t.Errorf("Unexpected error getting value - %v", e)
		} else {
			expected := "Hello Colin, John, Fred"
			if s, ok := v0.(string); ok && s != expected {
				t.Errorf("Incorrect value returned - expected '%v' , got '%v'", expected, s)
			}
		}
	}
}

func Test5(t *testing.T) {

	registerFunctions()

	future := New("Testing", "F3", "Colin", 3, 4, 5)

	_, e := future.GetResult()

	if e == nil {
		t.Errorf("Expected error but none returned")
	} else {
		errMsg := "interface conversion: interface is string, not int"
		if e.Error() != errMsg {
			t.Errorf("Expected error %v, but got %v", errMsg, e.Error())
		}
	}
}

func Test6(t *testing.T) {

	registerFunctions()

	future := New("Testing", "F4", 0, 100000000)

	// Call twice - should initially block, but then complete
	r1, e1 := future.GetResult()
	r2, e2 := future.GetResult()

	if e1 != nil || e2 != nil {
		if e1 != nil {
			t.Errorf("Unexpected error returned (e1) - %v", e1)
		}
		if e2 != nil {
			t.Errorf("Unexpected error returned (e2) - %v", e2)
		}
	} else if r1.NumValues() != 1 || r2.NumValues() != 1 {
		if r1.NumValues() != 1 {
			t.Errorf("Unexpected number of values returned (r1) - %v", r1.NumValues())
		}
		if r2.NumValues() != 1 {
			t.Errorf("Unexpected number of values returned (r2) - %v", r2.NumValues())
		}
	} else {
		v1, e1 := r1.Value(0)
		_, e2 := r2.Value(0)
		if e1 != nil || e2 != nil {
			if e1 != nil {
				t.Errorf("Unexpected error getting value (r1) - %v", e1)
			}
			if e2 != nil {
				t.Errorf("Unexpected error getting value (r2) - %v", e2)
			}
		} else {
			var expected float64 = (100000000 * 100000001) / 2
			if v, ok := v1.(float64); ok && v != expected {
				t.Errorf("Incorrect value returned - expected '%v' , got '%v', %v", expected, v, v-expected)
			}
		}
	}
}

func waitForFuture(f Future, c chan<- int, t *testing.T) {

	r, e := f.GetResult()

	if e != nil {
		t.Errorf("Unexpected error returned - %v", e)
	} else if r.NumValues() != 1 {
		t.Errorf("Unexpected number of values returned - %v", r.NumValues())
	} else {
		v0, e := r.Value(0)
		if e != nil {
			t.Errorf("Unexpected error getting value - %v", e)
		} else {
			var expected float64 = (10000000 * 10000001) / 2
			if s, ok := v0.(float64); !ok || (ok && s != expected) {
				t.Errorf("Incorrect value returned - expected '%v' , got '%v'", expected, s)
			}
		}
	}

	c <- 0
}

func Test7(t *testing.T) {

	registerFunctions()

	// Test for correct concurrent behaviour by using the same Future across
	// multiple goroutines
	num := 200
	c := make(chan int)

	future := New("Testing", "F4", 0, 10000000)

	for i := 0; i < num; i++ {
		go waitForFuture(future, c, t)
	}

	for {
		<-c
		num -= 1
		if num == 0 {
			break
		}
	}
}

func Test8(t *testing.T) {

	// This test allows the garbage collector to
	// call finalize() on the futures, demonstrating
	// that the notify() go routines exit (use cover to verify)
	runtime.GC()
	t1 := time.NewTimer(time.Second)
	<-t1.C
}
