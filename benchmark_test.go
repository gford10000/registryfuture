// Copyright 2014 Geoff Ford. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package registryFuture

import (
	"bitbucket.com/gford10000/functionRegistry"
	"testing"
)

func Benchmark1(b *testing.B) {

	b.StopTimer()

	f := func(i int) int {
		return i
	}

	functionRegistry.Register("Benchmark", "B1", f)

	b.StartTimer()

	for i := 0; i < b.N; i++ {

		future := New("Benchmark", "B1", 42)

		r, _ := future.GetResult()

		v, _ := r.Value(0)

		v0, _ := v.(int)

		if v0 != 42 {
			b.Error("Unexpected result obtained")
		}
	}
}
