// Copyright 2014 Geoff Ford. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// registryFuture provides syntactic sugar to simplify launching of goroutines to execute
// registered functions concurrently, by hiding the communication channels.
// The Future can be shared across multiple goroutines concurrently, and/or
// GetResult() called multiple times, without side effects.
package registryFuture

import (
	"bitbucket.com/gford10000/functionRegistry"
	"errors"
	"runtime"
)

// Future is the interface returned to New, allowing the original goroutine to continue
// processing whilst the specified function is invoked in a separate goroutine.
//
// A call to GetResult will cause the calling goroutine to block until the underlying
// functionRegistry.Invoke has completed.
// If the supplied arguments are incorrectly specified for the registered function, the
// error will provide details; if the function fails during invocation, then the
// Result.GetError() will contain the error as usual.
type Future interface {
	GetResult() (functionRegistry.Result, error) // Blocks until results are available
}

// future is the concrete implementation of Future
type future struct {
	i   chan int                // Request to receive the result
	o   chan int                // Signal to waiting goroutines
	err error                   // An error was generated during Invoke
	r   functionRegistry.Result // Result of Invoke
}

// start is called to launch the processing goroutine
func (f *future) start(namespace string, name string, args ...interface{}) {

	f.i = make(chan int)     // Request for notification by goroutine
	f.o = make(chan int, 10) // Reply to request by goroutine; buffer so that notify() loop is not blocked

	c := make(chan int)    // Notification of completion
	go notify(c, f.i, f.o) // Multiplexes notification to all waiting routines

	go run(c, f, namespace, name, args...) // Actual function invocation
}

// GetResult returns the result of the Invoke, blocking until this is ready
func (f *future) GetResult() (functionRegistry.Result, error) {
	f.i <- 1 // Signal that the routine wants results
	<-f.o    // Block until results are available
	return f.r, f.err
}

// finalizer ensures that the channels are closed once the future
// becomes unreachable, signalling to the notify() go routine to exit
func finalizer(i interface{}) {
	if f, ok := i.(*future); ok {
		close(f.i)
		close(f.o)
	}
}

// run performs the functionRegistry.Invoke(), ensuring that the notification is always processed
func run(c chan<- int, f *future, namespace string, name string, args ...interface{}) {

	// Always notify, even if a panic has been issued
	defer func() {
		if i := recover(); i != nil {
			c <- 0
			panic(i)
		} else {
			c <- 0
		}
	}()

	// Invoke can panic if the arguments are incorrect - trap and report
	defer func() {
		if i := recover(); i != nil {
			if e, ok := i.(error); ok {
				f.err = e
			} else if e, ok := i.(runtime.Error); ok {
				f.err = errors.New(e.Error())
			} else if s, ok := i.(string); ok {
				f.err = errors.New(s)
			} else {
				panic(i)
			}
		}
	}()

	// Actual call to function
	result, err := functionRegistry.Invoke(namespace, name, args...)

	// Normal processing
	f.r = result
	f.err = err
}

// notify multiplexes the notification that processing is completed to all requesting
// (and blocked) routines.  The loop will continue until the Future is garbage collected,
// at which point the future.i channel closure will signal the loop to exit.
func notify(c <-chan int, i <-chan int, o chan<- int) {
	numBlockedRoutines := 0
	notificationInvokeCompleted := false
	for {
		select {
		case <-c:
			{
				notificationInvokeCompleted = true
				for i := 0; i < numBlockedRoutines; i++ {
					o <- 0
				}
				numBlockedRoutines = 0
			}
		case _, ok := <-i:
			{
				if !ok {
					return // The Future has been disposed, all done
				}
				if !notificationInvokeCompleted {
					// Increment counter of waiting routines to be notified
					numBlockedRoutines += 1
				} else {
					// Invoke is completed, allow immediate resumption by requesting routine
					o <- 0
				}
			}
		}
	}
}

// New creates and starts a Future of the specified function and arguments
func New(namespace string, name string, args ...interface{}) Future {
	ret := new(future)
	runtime.SetFinalizer(ret, finalizer)
	ret.start(namespace, name, args...)
	return ret
}
