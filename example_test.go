// Copyright 2014 Geoff Ford. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
package registryFuture

import (
	"bitbucket.com/gford10000/functionRegistry"
	"fmt"
)

func ExampleGetResult() {

	// Normally registered elsewhere
	f := func() string {
		return "Hello, World"
	}
	functionRegistry.Register("Global", "Hello", f)

	// Create future to invoke the function
	future := New("Global", "Hello")

	// Wait on results
	result, _ := future.GetResult()

	// Unpack
	v, _ := result.Value(0)

	fmt.Printf("%v", v)
	// Output: Hello, World
}
